/****************************************************************************

** GNU Lesser General Public License Usage
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this
** file. Please review the following information to ensure the GNU Lesser
** General Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
**
**  Programmer: Andy Laberge
**  email: andylaberge@linux.com
** start development: 2012
**
**
****************************************************************************/

#include "tttuner.h"
#include "ui_tttuner.h"


int freqA;
int khzA;
int freqB;
int khzB;
int freqC;
int khzC;
int freqD;
int khzD;
int numTuners;  // number of tuners
int adaptersEn =0;

int sidcnt;

QObject *parent;

// four tuners should be enough- note: cpu i5 core ivy bridge 12 gig DDR3
QProcess *TTtunerA = new QProcess(parent);
QProcess *TTtunerB = new QProcess(parent);
QProcess *TTtunerC = new QProcess(parent);
QProcess *TTtunerD = new QProcess(parent);
QProcess *vlc = new QProcess(parent);

// QTimer *timerA ;

// globals watch out for these

QString globalChannel; // watch out for this
QString globalName; // name of service
QStringList streamid;
QStringList nameid;



tttuner::tttuner(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::tttuner)
{
    ui->setupUi(this);
    streamFlag=0x01;
    ota_enabled_flag=0;

// part of initiation
// this scans for tuner adapters
    QString path = "/dev/dvb"; // path for the adapters in Linux
    QDir dir;
    dir.setPath(path);

    QStringList adapters;
    check_adapter_FE();
    adapters=dir.entryList();
    numTuners = adapters.count();
    // look for physical adapter name and if found enable the tuner group
    if(numTuners>0)
    {
      //  numTuners=numTuners-2;
        ui->textBrowser1->clear();
   //     reconfCreate();
        loadReconf();
        if((adapters.contains("adapter0" )) && (ui->tunerNameA->text()=="ATSC"))
        {

                ui->tunerAgroupBox->setEnabled(true);
                adaptersEn++;
                scan_adapter="-a0";
                ota_enabled_flag+=1;

        }
        else
            ui->tunerAgroupBox->setEnabled(false);
        if((adapters.contains("adapter1")) && (ui->tunerNameB->text()=="ATSC"))
        {

                ui->tunerBgroupBox->setEnabled(true);
                adaptersEn++;
                scan_adapter="-a1";
                ota_enabled_flag+=2;
        }

        else
            ui->tunerBgroupBox->setEnabled(false);
        if((adapters.contains("adapter2")) && (ui->tunerNameC->text()=="ATSC"))
        {
            ui->tunerCgroupBox->setEnabled(true);
            adaptersEn++;
            scan_adapter="-a2";
            ota_enabled_flag+=4;
        }
        else
            ui->tunerCgroupBox->setEnabled(false);
        if((adapters.contains("adapter3")) && (ui->tunerNameD->text()=="ATSC"))
        {
            ui->tunerDgroupBox->setEnabled(true);
            adaptersEn++;
            scan_adapter="-a3";
            ota_enabled_flag+=8;
        }
        else
            ui->tunerDgroupBox->setEnabled(false);
     }
   else
   {
        ui->statusBar->showMessage("You have no tuner adapters"); // if there are no working adapters state so
        ui->centralWidget->setEnabled(false);
    }

    // added 18mar2013
    // checks for radio button pushed
    if(ui->createStreamsradioButt->isChecked()==true)
        ui->streamGroupBox->setEnabled(true);


}

tttuner::~tttuner()
{
    // when destroying the class kill any external processes
    TTtunerA->kill();
    TTtunerB->kill();
    TTtunerC->kill();
    TTtunerD->kill();
    vlc->kill();
    delete ui;
}

void tttuner::on_pushButton_clicked()
{


    if(ui->tuneTo_radioButt->isChecked()==true)
       TuneTo();
    if(ui->ATSCscanradioButt->isChecked()==true)
        ScanTuner();
    if(ui->DVBscanradioButton->isChecked()==true)
        ScanDVBtuner();
    if(ui->showStreamradioButt->isChecked()==true)
        ShowVideos();
    if(ui->createStreamsradioButt->isChecked()==true)
        createStreams();
}


void tttuner::TuneTo()
{

     if(ui->tunerAgroupBox->isEnabled()==true)
      {
        QString programA ="cvlc";
        QStringList argumentsA;
        QString text_freqA;



        freqA=khzA*1000; // freq is in hertz - "khz" and "freq" are global int

        text_freqA= text_freqA.setNum(freqA);
        // need the FrequencyA of the channel
        // atsc header says signal is 8VSB modulated
        // --dvb-budget-mode says to keep all the programs in the stream
        // without this we would just get the first program
// added : 24Feb2013 scale from .1 to 1

        argumentsA<<"atsc://frequency="+text_freqA<<"--dvb-budget-mode"<<"--dvb-adapter=0"<<"--zoom="+QString::number(ui->ScaleSpinBox->value());
        TTtunerA->start(programA,argumentsA);
       }
        if(ui->channelBspinBox->isEnabled()==true)
        {
                QString programB ="cvlc";
                QStringList argumentsB;
                QString text_freqB;



                freqB=khzB*1000; // freq is in hertz - "khz" and "freq" are global int

                text_freqB= text_freqB.setNum(freqB);
                // need the FrequencyA of the channel
                // atsc header says signal is 8VSB modulated
                // --dvb-budget-mode says to keep all the programs in the stream
                // without this we would just get the first program
// added : 24Feb2013 scale from .1 to 1
                argumentsB<<"atsc://frequency="+text_freqB<<"--dvb-budget-mode"<<"--dvb-adapter=1"<<"--zoom="+QString::number(ui->ScaleSpinBox->value());
                TTtunerB->start(programB,argumentsB);


        }
        if(ui->channelCspinBox->isEnabled()==true)
        {
                QString programC ="cvlc";
                QStringList argumentsC;
                QString text_freqC;



                freqC=khzC*1000; // freq is in hertz - "khz" and "freq" are global int

                text_freqC= text_freqC.setNum(freqC);
                // need the FrequencyA of the channel
                // atsc header says signal is 8VSB modulated
                // --dvb-budget-mode says to keep all the programs in the stream
                // without this we would just get the first program

 // added : 24Feb2013 scale from .1 to 1
                argumentsC<<"atsc://frequency="+text_freqC<<"--dvb-budget-mode"<<"--dvb-adapter=2"<<"--zoom="+QString::number(ui->ScaleSpinBox->value());
                TTtunerC->start(programC,argumentsC);


        }
        if(ui->channelDspinBox->isEnabled()==true)
        {
                QString programD ="cvlc";
                QStringList argumentsD;
                QString text_freqD;



                freqD=khzD*1000; // freq is in hertz - "khz" and "freq" are global int

                text_freqD= text_freqD.setNum(freqD);
                // need the FrequencyA of the channel
                // atsc header says signal is 8VSB modulated
                // --dvb-budget-mode says to keep all the programs in the stream
                // without this we would just get the first program
// added : 24Feb2013 scale from .1 to 1

                argumentsD<<"atsc://frequency="+text_freqD<<"--dvb-budget-mode"<<"--dvb-adapter=3"<<"--zoom="+QString::number(ui->ScaleSpinBox->value());
                TTtunerD->start(programD,argumentsD);


        }
}

void tttuner::ScanTuner()
{
         QString homepath = QDir::homePath();
         QString outform = "-o";
         QString formout = "zap"; //was zap
         QString numadapter ="-a";
         QString filename;

         filename = QFileDialog::getOpenFileName(this,tr("Open File"),homepath+"/tttuner1",
                                                                     tr(" (*ATSC.*)"));
                     if(!filename.isEmpty())
                     {
                         QFile file(filename);
                         if (!file.open(QFile::ReadOnly | QFile::Text))
                         {
                             QMessageBox::warning(this, tr("Recent Files"),
                                                  tr("Cannot read file %1:\n%2.")
                                                  .arg(filename)
                                                  .arg(file.errorString()));
                             return;
                         }
                      }
       if(adaptersEn>0)
        {
            ui->textBrowser->clear();
            ui->textBrowser1->clear();

            QString programA ="scan";// scan is a tuner scaning program
            QStringList argumentsA;

           // TTtunerA->setStandardOutputFile("/ttt_channels.conf");
            TTtunerA->setEnvironment(QProcess::systemEnvironment());
            TTtunerA->setProcessChannelMode(QProcess::MergedChannels);

            numadapter.append(QString::number(ui->scan_adapter_spinBox->value()));
            argumentsA<<outform<<formout<<scan_adapter<<numadapter<<filename; // arguments that scan for ATSC signals in the USA and produces config file

            TTtunerA->start(programA,argumentsA);
        // below takes the scan program and outputs it to a text browser in function read:Scan()
            TTtunerA->waitForStarted();
         //   connect(TTtunerA,SIGNAL(readyReadStandardError()),this,SLOT(endScan()));
            connect(TTtunerA,SIGNAL(readyReadStandardOutput()),this,SLOT(readScan()));
            connect(TTtunerA,SIGNAL(finished(int)),this,SLOT(stopTunerA()));


        }

 }
void tttuner::ScanDVBtuner()
{
         QString homepath = QDir::homePath();
         QString outform = "-o";
         QString formout = "zap";
         QString numadapter ="-a";

       if(adaptersEn>0)
        {
            ui->textBrowser->clear();
            ui->textBrowser1->clear();

            QString programA ="scan"; // scan is a tuner scaning program
            QStringList argumentsA;

           // TTtunerA->setStandardOutputFile("/ttt_channels.conf");
            TTtunerA->setEnvironment(QProcess::systemEnvironment());
            TTtunerA->setProcessChannelMode(QProcess::MergedChannels);

            numadapter.append(QString::number(ui->scan_adapter_spinBox->value()));
            argumentsA<<outform<<formout<<scan_adapter<<numadapter<<homepath+"/yakima-ATSC"; // arguments that scan for ATSC signals in the USA and produces config file

            TTtunerA->start(programA,argumentsA);
        // below takes the scan program and outputs it to a text browser in function read:Scan()
            TTtunerA->waitForStarted();
         //   connect(TTtunerA,SIGNAL(readyReadStandardError()),this,SLOT(endScan()));
            connect(TTtunerA,SIGNAL(readyReadStandardOutput()),this,SLOT(readScan()));
            connect(TTtunerA,SIGNAL(finished(int)),this,SLOT(stopTunerA()));


        }

 }

void tttuner::readScan()
{
    QString line;
    QString homepath = QDir::homePath();

    QProcess *TTtunerA = dynamic_cast<QProcess *>(sender());

    if(TTtunerA)
    ui->textBrowser->insertPlainText(TTtunerA->readAllStandardOutput());

    QFile file(homepath+"/tttuner.conf");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
           return ;


    QTextStream out(&file);
    line=out.readLine();
    out<<ui->textBrowser->toPlainText();
    file.close();
   //ui->textBrowser->hide();

}



void tttuner::ShowVideos()
{
        int cnt;
        QString line;

    //    int foundfreq;
        int foundprog;
        int linelen;
        int flag_en;

        QString programs;
        QString prognum;
        QString destination;
        QString destAll;
        QString homepathrd = QDir::homePath();

        QString homepath = QDir::homePath();

        homepath.append("/ttt_vlm.conf");


        QFile file(homepath);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                 return ;
        QString cntText;
        QString tunerNew;
        QString tunerSetup;
        QString tunerOutput;
        QString tunerControl;
        QString frequency;

        homepathrd.append("/tttuner.reconf");
        QFile filein(homepathrd);
        filein.open(QIODevice::ReadOnly | QIODevice::Text);

        QTextStream streamin(&filein);

        ui->textBrowser->clear();

        flag_en=ota_enabled_flag;
        for(cnt=0;cnt<adaptersEn;cnt++)
        {
           // cntText= QString::number(cnt);

            if((flag_en & 0x01)==0x01)
            {
                cntText="0";
                flag_en-=0x01;
            }
            else if((flag_en & 0x02)==0x02)
            {
                cntText="1";
                flag_en-=0x02;
            }
            else if((flag_en & 0x04)==0x04)
            {
                cntText="2";
                flag_en-=0x04;
            }
            else if((flag_en & 0x08)==0x08)
            {
                cntText="3";
                flag_en-=0x08;
            }

            tunerNew="new tuner_"+cntText+" broadcast enabled\n";

            ui->textBrowser->insertPlainText(tunerNew);

        }

         ui->textBrowser->insertPlainText("\n\n");


         flag_en=ota_enabled_flag;
         for(cnt=0;cnt<adaptersEn;cnt++)
         {
             // cntText= QString::number(cnt);
             if((flag_en & 0x01)==0x01)
             {
                 cntText="0";
                 flag_en-=0x01;
             }
             else if((flag_en & 0x02)==0x02)
             {
                 cntText="1";
                 flag_en-=0x02;
             }
             else if((flag_en & 0x04)==0x04)
             {
                 cntText="2";
                 flag_en-=0x04;
             }
             else if((flag_en & 0x08)==0x08)
             {
                 cntText="3";
                 flag_en-=0x08;
             }


             tunerSetup="setup tuner_"+cntText+" input 'atsc://'\n";
              ui->textBrowser->insertPlainText(tunerSetup);
              tunerSetup="setup tuner_"+cntText+" option dvb-adapter="+cntText+"\n";
              ui->textBrowser->insertPlainText(tunerSetup);
             // now insert the frequency of the selected channel for the adapter
             // adapter0 = tuner group a
              if(cntText=="0")
                 frequency=QString::number(ui->FrequencyA->intValue());
              else if(cntText=="1")
                  frequency=QString::number(ui->FrequencyB->intValue());
              else if(cntText=="2")
                  frequency=QString::number(ui->FrequencyC->intValue());
              else if(cntText=="3")
                  frequency=QString::number(ui->FrequencyD->intValue());

              tunerSetup="setup tuner_"+cntText+" option dvb-frequency="+frequency+"000\n";
              ui->textBrowser->insertPlainText(tunerSetup);
              tunerSetup="setup tuner_"+cntText+" option dvb-modulation=8VSB\n";
              ui->textBrowser->insertPlainText(tunerSetup);

              programs="programs=";
              while(!filein.atEnd())
              {
                  line = filein.readLine(0);
                  linelen=line.length();
                  if(line.contains(frequency)==true)
                  {
                     destination="dst=display,select= program=";
                     foundprog=line.indexOf("program=");
                     prognum=line.mid(foundprog+8,(linelen-1)-(foundprog+8));
                     prognum.append(",");
                     programs.append(prognum);
                     destination.append(prognum);
                     destAll.append(destination);


                  }


              }
              tunerSetup="setup tuner_"+cntText+" option "+programs+"\n" ;
              tunerOutput="setup tuner_"+cntText+" output '#duplicate{"+destAll+"}'\n";
              destAll.clear();
              filein.seek(0);

              ui->textBrowser->insertPlainText(tunerSetup);
              ui->textBrowser->insertPlainText("\n");
             ui->textBrowser->insertPlainText(tunerOutput);

             ui->textBrowser->insertPlainText("\n");


        }
          streamin.flush();
          filein.close();

         flag_en=ota_enabled_flag;
         for(cnt=0;cnt<adaptersEn;cnt++)
         {
            //cntText=QString::number(cnt);
             if((flag_en & 0x01)==0x01)
             {
                 cntText="0";
                 flag_en-=0x01;
             }
             else if((flag_en & 0x02)==0x02)
             {
                 cntText="1";
                 flag_en-=0x02;
             }
             else if((flag_en & 0x04)==0x04)
             {
                 cntText="2";
                 flag_en-=0x04;
             }
             else if((flag_en & 0x08)==0x08)
             {
                 cntText="3";
                 flag_en-=0x08;
             }

            tunerControl="control tuner_"+cntText+" play\n";
            ui->textBrowser->insertPlainText(tunerControl);

         }
         ui->textBrowser->insertPlainText("\n");

        QTextStream out(&file);

        out<<ui->textBrowser->toPlainText();
     //   file.close();
        statusBar()->showMessage(tr("File - %1 - Saved")
                .arg(homepath));



        QString program  ="cvlc";
        QStringList arguments;
// added : 24Feb2013 scale from .1 to 1

        arguments<<"--vlm-conf"<<homepath<<"--no-audio"<<"--zoom="+QString::number(ui->ScaleSpinBox->value());
        vlc->start(program,arguments);


}

void tttuner::createStreams()
{
    //this basically sets up the vlm-conf file
    // "new" creates the tuner stream
    // "setup" sets up stream
    // "control" starts,stops,pauses

    int cnt;
    QString line;


    int foundch;
    int foundcolon;
    int foundprog;
    int fnd_name;
    int fnd_name_colon;
    int linelen;
    int flag_en;

    QString programs;
    QString prognum;
    QString destinationA;
    QString destinationB;
    QString destAll;
    QString homepathrd = QDir::homePath();
    QString progname;

    QString homepath = QDir::homePath();
    sidcnt=0;
    streamid.clear();
    nameid.clear();

    homepath.append("/tttrtp_vlm.conf");


    QFile file(homepath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
             return ;
    QString cntText;
    QString rtpNew;
    QString rtpSetup;
    QString rtpOutput;
    QString rtpControl;
    QString frequency;
    QString ch;

    homepathrd.append("/tttuner.reconf");
    QFile filein(homepathrd);
    filein.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream streamin(&filein);

    ui->textBrowser->clear();

    flag_en=ota_enabled_flag;
    for(cnt=0;cnt<adaptersEn;cnt++)
    {
        if((flag_en & 0x01)==0x01)
        {
            cntText="0";
            flag_en-=0x01;
        }
        else if((flag_en & 0x02)==0x02)
        {
            cntText="1";
            flag_en-=0x02;
        }
        else if((flag_en & 0x04)==0x04)
        {
            cntText="2";
            flag_en-=0x04;
        }
        else if((flag_en & 0x08)==0x08)
        {
            cntText="3";
            flag_en-=0x08;
        }

        // cntText= QString::number(cnt);
        rtpNew="new rtp_"+cntText+" broadcast enabled\n";

        ui->textBrowser->insertPlainText(rtpNew);

    }

    ui->textBrowser->insertPlainText("\n");

    flag_en=ota_enabled_flag;
    for(cnt=0;cnt<adaptersEn;cnt++)
    {
        if((flag_en & 0x01)==0x01)
        {
            cntText="0";
            flag_en-=0x01;
        }
        else if((flag_en & 0x02)==0x02)
        {
            cntText="1";
            flag_en-=0x02;
        }
        else if((flag_en & 0x04)==0x04)
        {
            cntText="2";
            flag_en-=0x04;
        }
        else if((flag_en & 0x08)==0x08)
        {
            cntText="3";
            flag_en-=0x08;
        }

         //cntText= QString::number(cnt);
         rtpSetup="setup rtp_"+cntText+" input 'atsc://'\n";
         ui->textBrowser->insertPlainText(rtpSetup);
         rtpSetup="setup rtp_"+cntText+" option dvb-adapter="+cntText+"\n";
         ui->textBrowser->insertPlainText(rtpSetup);
        // now insert the frequency of the selected channel for the adapter
        // adapter0 = tuner group a
         if(cntText=="0")
            frequency=QString::number(ui->FrequencyA->intValue());
         else if(cntText=="1")
             frequency=QString::number(ui->FrequencyB->intValue());
         else if(cntText=="2")
             frequency=QString::number(ui->FrequencyC->intValue());
         else if(cntText=="3")
             frequency=QString::number(ui->FrequencyD->intValue());

         rtpSetup="setup rtp_"+cntText+" option dvb-frequency="+frequency+"000\n";
         ui->textBrowser->insertPlainText(rtpSetup);
         rtpSetup="setup rtp_"+cntText+" option dvb-modulation=8VSB\n";
         ui->textBrowser->insertPlainText(rtpSetup);

         programs="programs=";
         while(!filein.atEnd())
         {
             line = filein.readLine(0);
             linelen=line.length();
             if(line.contains(frequency)==true)
             {
                foundch=line.indexOf("CH ");
                foundcolon=line.indexOf(":");
                ch=line.mid(foundch+3,foundcolon-(foundch+3));
                fnd_name=line.indexOf("Name=");
                fnd_name_colon=line.indexOf(":",fnd_name+5);
                int namelen=(fnd_name_colon)-(fnd_name+5);
                progname=line.mid(fnd_name+5,namelen);

    // notes on mux
    // ps and raw don't seem to work well in this format

    // added: 23feb2013 the IPlineedit for editing IP address 127.0.0.1 is default localhost
                if(ui->rtpButton->isChecked()==true)
                {
                    destinationA="dst=rtp{mux=ts,ttl=12,dst="+ui->IPlineEdit->text()+",port="+ch;     // change to udp or rtp see below
                }
                else if(ui->udpButton->isChecked()==true)
                {
                    destinationA="dst=udp{mux=ts,ttl=12,dst="+ui->IPlineEdit->text()+":"+ch;
                }

                destinationB=",select= program=";

                foundprog=line.indexOf("program=");
                prognum=line.mid(foundprog+8,(linelen-1)-(foundprog+8));
                programs.append(prognum);
                programs.append(",");
                if(prognum.size()<2)
                {
                    destinationA.append("0");
                    ch.append("0");
                 }
                destinationA.append(prognum);
                ch.append(prognum);
              //
                if(ui->rtpButton->isChecked()==true)
                {
                    ch.prepend("rtp://"+ui->IPlineEdit->text()+":");  // change to udp ot rtp see above
                }
                if(ui->udpButton->isChecked()==true)
                {
                   // ch.prepend("udp://@:");
                    ch.prepend("udp://@"+ui->IPlineEdit->text()+":");
                }

                destinationA.append("}");
                destinationB.append(prognum+",");
                destinationA.append(destinationB);
                destAll.append(destinationA);
                streamid.append(ch);
                nameid.append(progname);
                sidcnt++;

             }


         }
         rtpSetup="setup rtp_"+cntText+" option "+programs+"\n" ;
         rtpOutput="setup rtp_"+cntText+" output '#duplicate{"+destAll+"}'\n";
         destAll.clear();
         filein.seek(0);

         ui->textBrowser->insertPlainText(rtpSetup);
         ui->textBrowser->insertPlainText("\n");
        ui->textBrowser->insertPlainText(rtpOutput);

        ui->textBrowser->insertPlainText("\n");


   }
    streamin.flush();
    filein.close();

   flag_en=ota_enabled_flag;
   for(cnt=0;cnt<adaptersEn;cnt++)
   {
       if((flag_en & 0x01)==0x01)
       {
           cntText="0";
           flag_en-=0x01;
       }
       else if((flag_en & 0x02)==0x02)
       {
           cntText="1";
           flag_en-=0x02;
       }
       else if((flag_en & 0x04)==0x04)
       {
           cntText="2";
           flag_en-=0x04;
       }
       else if((flag_en & 0x08)==0x08)
       {
           cntText="3";
           flag_en-=0x08;
       }

      //cntText=QString::number(cnt);
      rtpControl="control rtp_"+cntText+" play\n";
      ui->textBrowser->insertPlainText(rtpControl);

   }
   ui->textBrowser->insertPlainText("\n");
    QTextStream out(&file);

    out<<ui->textBrowser->toPlainText();
 //   file.close();
    statusBar()->showMessage(tr("File - %1 - Saved")
            .arg(homepath));

    QString program  ="cvlc";
    QStringList arguments;
// added : 24Feb2013 scale from .1 to 1

    arguments<<"--vlm-conf"<<homepath<<"--zoom="+QString::number(ui->ScaleSpinBox->value());
    vlc->start(program,arguments);

    // added : 25 feb 2013 clear the text browser

    ui->textBrowser->clear();
    ShowStreams();

}


void tttuner::ShowStreams()
{
    int i;
       int cnt;
       QDesktopWidget desktop;   // get desktop screen info ex: 1366x768

       int desktopHeight=desktop.geometry().height(); // height = 768
       int desktopWidth=desktop.geometry().width();  // width =1366


       QString vt;  //video title
       QString prog = "mpv"; // mpv or ffplay
       QString rtplocal; // local net 127.0.0.1
       QString tstring; // used for parsing data
       QString geo;  // geometry for individual views
       QString width; // width of view
       QString height; // height of view
       QString xpos; // view position x start
       QString ypos; // view position y start
       QString forceview = "--force-window"; //this is needed to give a window to audio
       QString mute = "--mute"; // comes on muted
       QString no_osc = "--no-osc"; // this takes out the onscreen control
       QString font_sz= "--osd-font-size=55";
       QString osd_bar = "--osd-bar"; // this allows an osd
       QString messag = "--osd-msg1=VBR ${=video-bitrate} ABR ${=audio-bitrate}"; //bitrates in cycles
       QString color = "--osd-color=0.0/1.0/0.0"; // video parameters
       QStringList argu; // argument list for QProcess
       QStringList list; // for parsing
       QString tnum;
       QString tport; // port number for UDP address
       QString rtpPort; //UDP address
       QString rtpadd;

       wide = ui->width_spinBox->value();
       high = ui->height_spinBox->value();
       posy = 20;



       width=QString::number(wide);
       height=QString::number(high);

       //build views
       for(i=0;i<sidcnt;i++)
       {
           geo="--geometry=";  // mpv geometry call

           xpos=QString::number(posx);
           ypos=QString::number(posy);

           geo.append(width+"x"+height);
           geo.append("+");
           geo.append(xpos);
           geo.append("+");
           geo.append(ypos);

           //    rtplocal="udp://@127.0.0.1:";


       argu.clear();
        rtplocal=streamid.at(i);
        vt="--title=";
        vt.append(nameid.at(i));

      //  argu<<rtplocal<<geo<<vt<<forceview<<no_osc<<font_sz<<osd_bar<<messag;
       // argu<<rtpadd<<scale<<nvd<<nqst<<nev<<vt;
         argu<<rtplocal<<geo<<vt<<forceview<<osd_bar<<messag<<color<<mute;
        QProcess *vlcrtp = new QProcess(this);
        vlcrtp->start(prog,argu);

        posx=posx+wide;
        if(posx>desktopWidth-(posx/4))
         {
            posx=20;
            posy=posy+(high+20+10);
     }
// add 25 feb 2013 show Hot Keys to Help
   // showHotkeys();
   }

}

void tttuner::on_channelAspinBox_valueChanged(int channel)
{


        QPalette* palette = new QPalette();
        palette->setColor(QPalette::Text,Qt::black);

        ui->channelAspinBox->setPalette(*palette);

        channel=ui->channelAspinBox->value();

        if(channel>=2 && channel<=4)
        {
            khzA=(channel*6+45)*1000;
            ui->FrequencyA->display(khzA);

        }
        if(channel>=5 && channel<=6)
        {
            khzA=(channel*6+49)*1000;
            ui->FrequencyA->display(khzA);

        }

        if(channel>=7 && channel<=13)
        {
            khzA=(channel*6+135)*1000;
            ui->FrequencyA->display(khzA);

        }
        if(channel>=14 && channel<=51)
        {
            khzA=(channel*6+389)*1000;
            ui->FrequencyA->display(khzA);

        }

        //channelGood(channel);

            if(channelGood(channel)==true)     // see if channel is exist
            {
               palette->setColor(QPalette::Text,Qt::red);
               ui->channelAspinBox->setPalette(*palette);
               ui->tunerNameA->clear();
               ui->tunerNameA->insert(globalName);
            }

}

void tttuner::on_channelBspinBox_valueChanged(int channel)
{
    //    this will take the channel and convert it to kilohertz

        QPalette* palette = new QPalette();
        palette->setColor(QPalette::Text,Qt::black);

        ui->channelBspinBox->setPalette(*palette);

        channel=ui->channelBspinBox->value();

        if(channel>=2 && channel<=4)
        {
            khzB=(channel*6+45)*1000;
            ui->FrequencyB->display(khzB);

        }
        if(channel>=5 && channel<=6)
        {
            khzB=(channel*6+49)*1000;
            ui->FrequencyB->display(khzB);

        }

        if(channel>=7 && channel<=13)
        {
            khzB=(channel*6+135)*1000;
            ui->FrequencyB->display(khzB);

        }
        if(channel>=14 && channel<=51)
        {
            khzB=(channel*6+389)*1000;
            ui->FrequencyB->display(khzB);

        }
        //channelGood(channel);

            if(channelGood(channel)==true)     // see if channel is exist
            {
               palette->setColor(QPalette::Text,Qt::red);
               ui->channelBspinBox->setPalette(*palette);
               ui->tunerNameB->clear();
               ui->tunerNameB->insert(globalName);
            }
}

void tttuner::on_channelCspinBox_valueChanged(int channel)
{
    //    this will take the channel and convert it to kilohertz

        QPalette* palette = new QPalette();
        palette->setColor(QPalette::Text,Qt::black);

        ui->channelCspinBox->setPalette(*palette);

        channel=ui->channelCspinBox->value();

        if(channel>=2 && channel<=4)
        {
            khzC=(channel*6+45)*1000;
            ui->FrequencyC->display(khzC);

        }
        if(channel>=5 && channel<=6)
        {
            khzC=(channel*6+49)*1000;
            ui->FrequencyC->display(khzC);

        }

        if(channel>=7 && channel<=13)
        {
            khzC=(channel*6+135)*1000;
            ui->FrequencyC->display(khzC);

        }
        if(channel>=14 && channel<=51)
        {
            khzC=(channel*6+389)*1000;
            ui->FrequencyC->display(khzC);

        }
        //channelGood(channel);

            if(channelGood(channel)==true)     // see if channel is exist
            {
               palette->setColor(QPalette::Text,Qt::red);
               ui->channelCspinBox->setPalette(*palette);
               ui->tunerNameC->clear();
               ui->tunerNameC->insert(globalName);
            }
}

void tttuner::on_channelDspinBox_valueChanged(int channel)
{
    //    this will take the channel and convert it to kilohertz

        QPalette* palette = new QPalette();
        palette->setColor(QPalette::Text,Qt::black);

        ui->channelDspinBox->setPalette(*palette);

        channel=ui->channelDspinBox->value();

        if(channel>=2 && channel<=4)
        {
            khzD=(channel*6+45)*1000;
            ui->FrequencyD->display(khzD);

        }
        if(channel>=5 && channel<=6)
        {
            khzD=(channel*6+49)*1000;
            ui->FrequencyD->display(khzD);

        }

        if(channel>=7 && channel<=13)
        {
            khzD=(channel*6+135)*1000;
            ui->FrequencyD->display(khzD);

        }
        if(channel>=14 && channel<=51)
        {
            khzD=(channel*6+389)*1000;
            ui->FrequencyD->display(khzD);

        }
        //channelGood(channel);

            if(channelGood(channel)==true)     // see if channel is exist
            {
               palette->setColor(QPalette::Text,Qt::red);
               ui->channelDspinBox->setPalette(*palette);
               ui->tunerNameD->clear();
               ui->tunerNameD->insert(globalName);
            }
}

void tttuner::endScan()
{
   // QProcess *TTtunerA = dynamic_cast<QProcess *>(sender());



}


void tttuner::stopTunerA()
{
    TTtunerA->waitForFinished();
    if(TTtunerA->state() != QProcess::NotRunning);
        TTtunerA->kill();
       // ui->textBrowser->clear();
        ui->tuneTo_radioButt->setChecked(true);

        reconfCreate();
}

void tttuner::reconfCreate()
{
    // chop down the tttuner.conf file reorganize and create tttuner.reconf
    int foundtuneto;
    int foundcolon;
    int found8V;
    int foundlastcolon;
    int len;
    int proglen;
    int rmvlen;

   // QStringList channelsVirtual;


    QString temp;
 //   QString temp2;

    QString line;
    QString homepath = QDir::homePath();
    QString program;
    ui->textBrowser1->clear(); // clear the text editor

    // **********************************************
    // BIG NOTE TO MYSELF  BIG NOTE TO MYSELF
    // MAKE SURE YOU CHANGE THE "tttuner1.conf"
    // BACK TO                  "tttuner.conf"
    // END OF NOTE   END OF NOTE
    // ************************************************

    QFile filein(homepath+"/tttuner.conf");
    if(!filein.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream streamin(&filein);

    line = filein.readLine(0);

 //   the routine below needs to be done in a
 // succint manner so that errors do not occur
 // in the ouput

    while(!filein.atEnd())
    {
        line = filein.readLine(0);
        foundtuneto=line.indexOf("Done");
        if(foundtuneto!=-1)
        {
            while(!filein.atEnd())
            {
                line = filein.readLine(0);
                len = line.length()-1;
                foundlastcolon=line.lastIndexOf(":");
                proglen=len-foundlastcolon;
                program = line.right(proglen);
                program.prepend(":program=");
                found8V=line.indexOf(":8V");
                rmvlen=len-found8V+1;
                line.remove(found8V,rmvlen);
                line.append(program);
                line.prepend("Name=");
                foundcolon= line.indexOf(":");
                temp=line.mid(foundcolon+1,3);
                cnvfrqtchnl(temp);
                line.insert(foundcolon+1,"f=");
                line.prepend(globalChannel);


                ui->textBrowser1->insertPlainText(line);

            }
        }
    }
//   QString homepath = QDir::homePath();

    homepath.append("/tttuner.reconf");


    QFile file(homepath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
           return ;


    QTextStream out(&file);

    out<<ui->textBrowser1->toPlainText();
 //   file.close();
    statusBar()->showMessage(tr("File - %1 - Saved")
            .arg(homepath));
    streamin.flush();
    out.flush();

}
void tttuner::cnvfrqtchnl(QString temp)
{
    if(temp=="570")
        globalChannel="CH 2:";
    else if(temp=="630")
        globalChannel="CH 3:";
    else if(temp=="690")
        globalChannel="CH 4:";
    else if(temp=="790")
        globalChannel="CH 5:";
    else if(temp=="850")
        globalChannel="CH 6:";

    else
    {

        int freq1;
        int freq2;

        freq1=temp.toInt();

        if(freq1<472)
            freq2=freq1-135;
        else
            freq2=freq1-389;

        freq1=freq2/6;
        globalChannel=QString::number(freq1);
        globalChannel.prepend("CH ");
        globalChannel.append(":");
    }
}


void tttuner::on_tuneTo_radioButt_clicked()
{
    if(ui->tuneTo_radioButt->isEnabled()==true)
    {
        // put the
    }
}

void tttuner::loadReconf()
{
    QString  line;
    QString homepath = QDir::homePath();
    homepath.append("/tttuner.reconf");
    QFile filein(homepath);
    filein.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream streamin(&filein);

    while(!filein.atEnd())
    {
        line=filein.readLine(0);
        ui->textBrowser1->insertPlainText(line);
    }


}

void tttuner::on_showStreamradioButt_clicked()
{



}

bool tttuner::channelGood(int value)
{
    QString channel=QString::number(value);
   // ui->textBrowser->insertPlainText(channel);

    QString line;
 //   QString channel;
    QString lineChannel;

    int foundCH;
    int found;
    int foundName;  // index for "Name="
    int found2;     // second colon ":f"
    int lineChannelsize;

    QString homepath = QDir::homePath();
    homepath.append("/tttuner.reconf");
    QFile filein(homepath);
    filein.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream streamin(&filein);

    while(!filein.atEnd())
    {
        line = filein.readLine(0);
        foundCH=line.indexOf("CH ");
        found=line.indexOf(":");
        foundName=line.indexOf("Name=");
        found2=line.indexOf(":f");

        lineChannelsize=found-(foundCH+3);
        lineChannel= line.mid(foundCH+3,lineChannelsize);

        globalName=line.mid(foundName+5,(found2)-(foundName+5));

        if(lineChannel==channel)
        {
            filein.close();
            streamin.flush();
            return true;
        }

    }
        filein.close();
        streamin.flush();
        return false;
}

void tttuner::check_adapter_FE()
{

    ui->textBrowser->clear();
    for(int cnt=0;cnt<4;cnt++)



    {
        fetype=-1;
        fename.clear();
        QString adapter = QString::number(cnt);
        open_fd(cnt);

        switch (fetype)
        {

        case 0:
        {
            stuff="QPSK";
            break;
        }
        case 1:
        {
            stuff="QAM";
            break;
        }
        case 2:
        {
            stuff="OFDM";
            break;
        }

        case 3:
        {
            stuff="ATSC";
            break;
        }
        case -1:
        {
            stuff="NONE";
            break;
        }
        }


        ui->textBrowser->insertPlainText("Adapter"+adapter+" is "+fename+" "+stuff+"\n");
        if(cnt==0)
        {
            ui->tunerNameA->insert(stuff);
            if(fetype!=-1)
                ui->tunerAgroupBox->setEnabled(true);
        }
        if(cnt==1)
        {
            ui->tunerNameB->insert(stuff);
            if(fetype!=-1)
                ui->tunerBgroupBox->setEnabled(true);
        }
        if(cnt==2)
        {
            ui->tunerNameC->insert(stuff);
            if(fetype!=-1)
                ui->tunerCgroupBox->setEnabled(true);
        }
        if(cnt==3)
        {
            ui->tunerNameD->insert(stuff);
            if(fetype!=-1)
                ui->tunerDgroupBox->setEnabled(true);
        }
        // close(fd);

    }
}

void tttuner::open_fd(int cnt)
{
    switch(cnt)
    {

        case 0:
        {
        frontend_name0 = "/dev/dvb/adapter0/frontend0";

        if((fd0=open(frontend_name0, O_RDONLY | O_NONBLOCK))<0)
        {
            ui->textBrowser->insertPlainText("Failed ");
            fetype=-1;


        }
        else
        {
            ui->textBrowser->insertPlainText("Opened ");

            if(ioctl(fd0,FE_GET_INFO,&fe_stuff0)!=-1)
            {
                fetype=fe_stuff0.type;
                fename=fe_stuff0.name;
                fd0=close();
            }
        }
        break;
      }

    case 1:
        {
        frontend_name1 = "/dev/dvb/adapter1/frontend0";

        if((fd1=open(frontend_name1, O_RDONLY | O_NONBLOCK))<0)
        {
            ui->textBrowser->insertPlainText("Failed ");
            fetype=-1;
        }
        else
        {
            ui->textBrowser->insertPlainText("Opened ");

            if(ioctl(fd1,FE_GET_INFO,&fe_stuff1)!=-1)
            {
                fetype=fe_stuff1.type;
                fename=fe_stuff1.name;
                fd0=close();

            }

        }
        break;
      }

    case 2:
        {
        frontend_name2 = "/dev/dvb/adapter2/frontend0";

        if((fd2=open(frontend_name2, O_RDONLY | O_NONBLOCK))<0)
        {
            ui->textBrowser->insertPlainText("Failed ");
            fetype=-1;
        }
        else
        {
            ui->textBrowser->insertPlainText("Opened ");

            if(ioctl(fd2,FE_GET_INFO,&fe_stuff2)!=-1)
            {
                fetype = fe_stuff2.type;
                fename=fe_stuff2.name;
                fd2=close();

            }

        }
        break;
      }
    case 3:
        {
        frontend_name3 = "/dev/dvb/adapter3/frontend0";

        if((fd3=open(frontend_name3, O_RDONLY | O_NONBLOCK))<0)
        {
            ui->textBrowser->insertPlainText("Failed ");
            fetype=-1;

        }
        else
        {
            ui->textBrowser->insertPlainText("Opened ");

            if(ioctl(fd3,FE_GET_INFO,&fe_stuff3)!=-1)
            {
                fetype = fe_stuff3.type;
                fename=fe_stuff3.name;
                fd3=close();
            }

        }
        break;
      }
    }

}
void tttuner::on_createStreamsradioButt_toggled(bool checked)
{
    if(checked==true)
        ui->streamGroupBox->setEnabled(true);
    if(checked==false)
        ui->streamGroupBox->setEnabled(false);


}

void tttuner::on_rtpButton_toggled(bool checked)
{
    if(checked==true)
       streamFlag=0x01;
    else
        streamFlag=0x02;
}

void tttuner::on_pushButton_2_clicked()
{





    //TTtunerA->kill();
   // TTtunerB->kill();
   // TTtunerC->kill();
   // TTtunerD->kill();

}

void tttuner::showHotkeys()
{
    ui->textBrowser->insertPlainText(" Hot Keys work on last video that the mouse was clicked on!\n\n");
    ui->textBrowser->insertPlainText(" f will toggle full screen \n\n");
    ui->textBrowser->insertPlainText(" Ctrl-Up Ctrl-Down or the mouse wheel for volume\n\n");
    ui->textBrowser->insertPlainText(" Shift -r toggles record on off recording in UDP will cause\n");
    ui->textBrowser->insertPlainText(" drop-outs RTP produces better results!\n");

}
