/********************************************************************************
** Form generated from reading UI file 'tttuner.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TTTUNER_H
#define UI_TTTUNER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_tttuner
{
public:
    QWidget *centralWidget;
    QGroupBox *FunctiongroupBox;
    QRadioButton *tuneTo_radioButt;
    QRadioButton *ATSCscanradioButt;
    QRadioButton *showStreamradioButt;
    QPushButton *pushButton;
    QRadioButton *createStreamsradioButt;
    QRadioButton *DVBscanradioButton;
    QSpinBox *scan_adapter_spinBox;
    QLabel *label_13;
    QGroupBox *tunerAgroupBox;
    QSpinBox *channelAspinBox;
    QLabel *label;
    QLCDNumber *FrequencyA;
    QLabel *label_2;
    QLineEdit *tunerNameA;
    QGroupBox *tunerBgroupBox;
    QSpinBox *channelBspinBox;
    QLCDNumber *FrequencyB;
    QLabel *label_3;
    QLabel *label_4;
    QLineEdit *tunerNameB;
    QTextBrowser *textBrowser;
    QGroupBox *tunerCgroupBox;
    QSpinBox *channelCspinBox;
    QLCDNumber *FrequencyC;
    QLabel *label_5;
    QLabel *label_6;
    QLineEdit *tunerNameC;
    QGroupBox *tunerDgroupBox;
    QSpinBox *channelDspinBox;
    QLCDNumber *FrequencyD;
    QLabel *label_7;
    QLabel *label_8;
    QLineEdit *tunerNameD;
    QTextBrowser *textBrowser1;
    QGroupBox *streamGroupBox;
    QRadioButton *rtpButton;
    QRadioButton *udpButton;
    QLineEdit *IPlineEdit;
    QSpinBox *width_spinBox;
    QSpinBox *height_spinBox;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QDoubleSpinBox *ScaleSpinBox;
    QLabel *label_12;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *tttuner)
    {
        if (tttuner->objectName().isEmpty())
            tttuner->setObjectName(QStringLiteral("tttuner"));
        tttuner->setWindowModality(Qt::NonModal);
        tttuner->resize(867, 423);
        centralWidget = new QWidget(tttuner);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        FunctiongroupBox = new QGroupBox(centralWidget);
        FunctiongroupBox->setObjectName(QStringLiteral("FunctiongroupBox"));
        FunctiongroupBox->setGeometry(QRect(10, 0, 121, 191));
        tuneTo_radioButt = new QRadioButton(FunctiongroupBox);
        tuneTo_radioButt->setObjectName(QStringLiteral("tuneTo_radioButt"));
        tuneTo_radioButt->setGeometry(QRect(0, 60, 105, 21));
        tuneTo_radioButt->setChecked(false);
        ATSCscanradioButt = new QRadioButton(FunctiongroupBox);
        ATSCscanradioButt->setObjectName(QStringLiteral("ATSCscanradioButt"));
        ATSCscanradioButt->setEnabled(true);
        ATSCscanradioButt->setGeometry(QRect(0, 80, 105, 21));
        ATSCscanradioButt->setChecked(false);
        showStreamradioButt = new QRadioButton(FunctiongroupBox);
        showStreamradioButt->setObjectName(QStringLiteral("showStreamradioButt"));
        showStreamradioButt->setGeometry(QRect(0, 40, 121, 21));
        pushButton = new QPushButton(FunctiongroupBox);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(0, 160, 111, 27));
        createStreamsradioButt = new QRadioButton(FunctiongroupBox);
        createStreamsradioButt->setObjectName(QStringLiteral("createStreamsradioButt"));
        createStreamsradioButt->setEnabled(true);
        createStreamsradioButt->setGeometry(QRect(0, 20, 121, 21));
        createStreamsradioButt->setChecked(true);
        DVBscanradioButton = new QRadioButton(FunctiongroupBox);
        DVBscanradioButton->setObjectName(QStringLiteral("DVBscanradioButton"));
        DVBscanradioButton->setGeometry(QRect(0, 100, 104, 20));
        scan_adapter_spinBox = new QSpinBox(FunctiongroupBox);
        scan_adapter_spinBox->setObjectName(QStringLiteral("scan_adapter_spinBox"));
        scan_adapter_spinBox->setGeometry(QRect(0, 130, 47, 24));
        scan_adapter_spinBox->setMaximum(3);
        label_13 = new QLabel(FunctiongroupBox);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(50, 130, 71, 21));
        tunerAgroupBox = new QGroupBox(centralWidget);
        tunerAgroupBox->setObjectName(QStringLiteral("tunerAgroupBox"));
        tunerAgroupBox->setEnabled(false);
        tunerAgroupBox->setGeometry(QRect(330, 0, 120, 191));
        channelAspinBox = new QSpinBox(tunerAgroupBox);
        channelAspinBox->setObjectName(QStringLiteral("channelAspinBox"));
        channelAspinBox->setGeometry(QRect(0, 20, 52, 25));
        QPalette palette;
        QBrush brush(QColor(170, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::HighlightedText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush);
        channelAspinBox->setPalette(palette);
        QFont font;
        font.setKerning(true);
        channelAspinBox->setFont(font);
        channelAspinBox->setCursor(QCursor(Qt::ArrowCursor));
        channelAspinBox->setMouseTracking(true);
        channelAspinBox->setFocusPolicy(Qt::WheelFocus);
        channelAspinBox->setContextMenuPolicy(Qt::DefaultContextMenu);
        channelAspinBox->setAutoFillBackground(false);
        channelAspinBox->setInputMethodHints(Qt::ImhDigitsOnly);
        channelAspinBox->setWrapping(false);
        channelAspinBox->setReadOnly(false);
        channelAspinBox->setKeyboardTracking(true);
        channelAspinBox->setMinimum(2);
        channelAspinBox->setMaximum(51);
        channelAspinBox->setValue(2);
        label = new QLabel(tunerAgroupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(60, 20, 57, 15));
        FrequencyA = new QLCDNumber(tunerAgroupBox);
        FrequencyA->setObjectName(QStringLiteral("FrequencyA"));
        FrequencyA->setGeometry(QRect(0, 50, 71, 23));
        FrequencyA->setFrameShape(QFrame::Panel);
        FrequencyA->setFrameShadow(QFrame::Sunken);
        FrequencyA->setLineWidth(3);
        FrequencyA->setDigitCount(6);
        FrequencyA->setSegmentStyle(QLCDNumber::Flat);
        FrequencyA->setProperty("value", QVariant(57000));
        FrequencyA->setProperty("intValue", QVariant(57000));
        label_2 = new QLabel(tunerAgroupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(80, 50, 57, 15));
        tunerNameA = new QLineEdit(tunerAgroupBox);
        tunerNameA->setObjectName(QStringLiteral("tunerNameA"));
        tunerNameA->setGeometry(QRect(0, 80, 113, 25));
        tunerBgroupBox = new QGroupBox(centralWidget);
        tunerBgroupBox->setObjectName(QStringLiteral("tunerBgroupBox"));
        tunerBgroupBox->setEnabled(false);
        tunerBgroupBox->setGeometry(QRect(470, 0, 120, 191));
        channelBspinBox = new QSpinBox(tunerBgroupBox);
        channelBspinBox->setObjectName(QStringLiteral("channelBspinBox"));
        channelBspinBox->setGeometry(QRect(0, 20, 52, 25));
        channelBspinBox->setMinimum(2);
        channelBspinBox->setMaximum(51);
        channelBspinBox->setValue(2);
        FrequencyB = new QLCDNumber(tunerBgroupBox);
        FrequencyB->setObjectName(QStringLiteral("FrequencyB"));
        FrequencyB->setGeometry(QRect(0, 50, 64, 23));
        FrequencyB->setFrameShape(QFrame::Panel);
        FrequencyB->setFrameShadow(QFrame::Sunken);
        FrequencyB->setLineWidth(3);
        FrequencyB->setDigitCount(6);
        FrequencyB->setSegmentStyle(QLCDNumber::Flat);
        FrequencyB->setProperty("value", QVariant(57000));
        label_3 = new QLabel(tunerBgroupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(60, 20, 57, 15));
        label_4 = new QLabel(tunerBgroupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(70, 50, 57, 15));
        tunerNameB = new QLineEdit(tunerBgroupBox);
        tunerNameB->setObjectName(QStringLiteral("tunerNameB"));
        tunerNameB->setGeometry(QRect(0, 80, 113, 25));
        textBrowser = new QTextBrowser(centralWidget);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(0, 200, 421, 192));
        textBrowser->setFrameShadow(QFrame::Sunken);
        textBrowser->setLineWidth(3);
        tunerCgroupBox = new QGroupBox(centralWidget);
        tunerCgroupBox->setObjectName(QStringLiteral("tunerCgroupBox"));
        tunerCgroupBox->setEnabled(false);
        tunerCgroupBox->setGeometry(QRect(610, 0, 120, 191));
        channelCspinBox = new QSpinBox(tunerCgroupBox);
        channelCspinBox->setObjectName(QStringLiteral("channelCspinBox"));
        channelCspinBox->setGeometry(QRect(0, 20, 52, 25));
        channelCspinBox->setMinimum(2);
        channelCspinBox->setMaximum(51);
        FrequencyC = new QLCDNumber(tunerCgroupBox);
        FrequencyC->setObjectName(QStringLiteral("FrequencyC"));
        FrequencyC->setGeometry(QRect(0, 50, 64, 23));
        FrequencyC->setFrameShape(QFrame::Panel);
        FrequencyC->setFrameShadow(QFrame::Sunken);
        FrequencyC->setLineWidth(3);
        FrequencyC->setDigitCount(6);
        FrequencyC->setSegmentStyle(QLCDNumber::Flat);
        FrequencyC->setProperty("value", QVariant(57000));
        label_5 = new QLabel(tunerCgroupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(60, 20, 57, 15));
        label_6 = new QLabel(tunerCgroupBox);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(70, 50, 57, 15));
        tunerNameC = new QLineEdit(tunerCgroupBox);
        tunerNameC->setObjectName(QStringLiteral("tunerNameC"));
        tunerNameC->setGeometry(QRect(0, 80, 113, 25));
        tunerDgroupBox = new QGroupBox(centralWidget);
        tunerDgroupBox->setObjectName(QStringLiteral("tunerDgroupBox"));
        tunerDgroupBox->setEnabled(false);
        tunerDgroupBox->setGeometry(QRect(740, 0, 120, 191));
        channelDspinBox = new QSpinBox(tunerDgroupBox);
        channelDspinBox->setObjectName(QStringLiteral("channelDspinBox"));
        channelDspinBox->setGeometry(QRect(0, 20, 52, 25));
        channelDspinBox->setMinimum(2);
        channelDspinBox->setMaximum(51);
        FrequencyD = new QLCDNumber(tunerDgroupBox);
        FrequencyD->setObjectName(QStringLiteral("FrequencyD"));
        FrequencyD->setGeometry(QRect(0, 50, 64, 23));
        FrequencyD->setFrameShape(QFrame::Panel);
        FrequencyD->setFrameShadow(QFrame::Sunken);
        FrequencyD->setLineWidth(3);
        FrequencyD->setDigitCount(6);
        FrequencyD->setSegmentStyle(QLCDNumber::Flat);
        FrequencyD->setProperty("value", QVariant(57000));
        label_7 = new QLabel(tunerDgroupBox);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(60, 20, 57, 15));
        label_8 = new QLabel(tunerDgroupBox);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(70, 50, 57, 15));
        tunerNameD = new QLineEdit(tunerDgroupBox);
        tunerNameD->setObjectName(QStringLiteral("tunerNameD"));
        tunerNameD->setGeometry(QRect(0, 80, 113, 25));
        textBrowser1 = new QTextBrowser(centralWidget);
        textBrowser1->setObjectName(QStringLiteral("textBrowser1"));
        textBrowser1->setGeometry(QRect(440, 200, 421, 191));
        textBrowser1->setTabChangesFocus(true);
        streamGroupBox = new QGroupBox(centralWidget);
        streamGroupBox->setObjectName(QStringLiteral("streamGroupBox"));
        streamGroupBox->setEnabled(true);
        streamGroupBox->setGeometry(QRect(130, 0, 181, 201));
        rtpButton = new QRadioButton(streamGroupBox);
        rtpButton->setObjectName(QStringLiteral("rtpButton"));
        rtpButton->setGeometry(QRect(0, 20, 61, 21));
        rtpButton->setChecked(false);
        udpButton = new QRadioButton(streamGroupBox);
        udpButton->setObjectName(QStringLiteral("udpButton"));
        udpButton->setGeometry(QRect(0, 40, 51, 21));
        udpButton->setChecked(true);
        IPlineEdit = new QLineEdit(streamGroupBox);
        IPlineEdit->setObjectName(QStringLiteral("IPlineEdit"));
        IPlineEdit->setGeometry(QRect(10, 60, 171, 25));
        width_spinBox = new QSpinBox(streamGroupBox);
        width_spinBox->setObjectName(QStringLiteral("width_spinBox"));
        width_spinBox->setGeometry(QRect(10, 100, 47, 24));
        width_spinBox->setMaximum(640);
        width_spinBox->setSingleStep(10);
        width_spinBox->setValue(460);
        height_spinBox = new QSpinBox(streamGroupBox);
        height_spinBox->setObjectName(QStringLiteral("height_spinBox"));
        height_spinBox->setGeometry(QRect(70, 100, 47, 24));
        height_spinBox->setMaximum(480);
        height_spinBox->setSingleStep(10);
        height_spinBox->setValue(250);
        label_9 = new QLabel(streamGroupBox);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 130, 41, 16));
        label_10 = new QLabel(streamGroupBox);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(60, 110, 16, 16));
        label_11 = new QLabel(streamGroupBox);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(70, 130, 41, 16));
        ScaleSpinBox = new QDoubleSpinBox(streamGroupBox);
        ScaleSpinBox->setObjectName(QStringLiteral("ScaleSpinBox"));
        ScaleSpinBox->setGeometry(QRect(10, 150, 66, 24));
        ScaleSpinBox->setMinimum(0.1);
        ScaleSpinBox->setMaximum(1);
        ScaleSpinBox->setSingleStep(0.05);
        ScaleSpinBox->setValue(0.3);
        label_12 = new QLabel(streamGroupBox);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(10, 180, 59, 14));
        tttuner->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(tttuner);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        tttuner->setStatusBar(statusBar);
        QWidget::setTabOrder(pushButton, channelAspinBox);
        QWidget::setTabOrder(channelAspinBox, channelBspinBox);
        QWidget::setTabOrder(channelBspinBox, channelCspinBox);
        QWidget::setTabOrder(channelCspinBox, channelDspinBox);
        QWidget::setTabOrder(channelDspinBox, tuneTo_radioButt);
        QWidget::setTabOrder(tuneTo_radioButt, showStreamradioButt);
        QWidget::setTabOrder(showStreamradioButt, ATSCscanradioButt);
        QWidget::setTabOrder(ATSCscanradioButt, textBrowser);
        QWidget::setTabOrder(textBrowser, textBrowser);

        retranslateUi(tttuner);

        QMetaObject::connectSlotsByName(tttuner);
    } // setupUi

    void retranslateUi(QMainWindow *tttuner)
    {
        tttuner->setWindowTitle(QApplication::translate("tttuner", "TTTuner", Q_NULLPTR));
        FunctiongroupBox->setTitle(QApplication::translate("tttuner", "Tuner Function", Q_NULLPTR));
        tuneTo_radioButt->setText(QApplication::translate("tttuner", "Tune To", Q_NULLPTR));
        ATSCscanradioButt->setText(QApplication::translate("tttuner", "ATSC Scan", Q_NULLPTR));
        showStreamradioButt->setText(QApplication::translate("tttuner", "Show Video", Q_NULLPTR));
        pushButton->setText(QApplication::translate("tttuner", "Tune", Q_NULLPTR));
        createStreamsradioButt->setText(QApplication::translate("tttuner", "Create Streams", Q_NULLPTR));
        DVBscanradioButton->setText(QApplication::translate("tttuner", "DVB Scan", Q_NULLPTR));
        label_13->setText(QApplication::translate("tttuner", "ScanAdap", Q_NULLPTR));
        tunerAgroupBox->setTitle(QApplication::translate("tttuner", "Adapter0", Q_NULLPTR));
        label->setText(QApplication::translate("tttuner", "Channel", Q_NULLPTR));
        label_2->setText(QApplication::translate("tttuner", "kHz", Q_NULLPTR));
        tunerBgroupBox->setTitle(QApplication::translate("tttuner", "Adapter1", Q_NULLPTR));
        label_3->setText(QApplication::translate("tttuner", "Channel", Q_NULLPTR));
        label_4->setText(QApplication::translate("tttuner", "kHz", Q_NULLPTR));
        textBrowser->setDocumentTitle(QString());
        tunerCgroupBox->setTitle(QApplication::translate("tttuner", "Adapter2", Q_NULLPTR));
        label_5->setText(QApplication::translate("tttuner", "Channel", Q_NULLPTR));
        label_6->setText(QApplication::translate("tttuner", "kHz", Q_NULLPTR));
        tunerDgroupBox->setTitle(QApplication::translate("tttuner", "Adapter3", Q_NULLPTR));
        label_7->setText(QApplication::translate("tttuner", "Channel", Q_NULLPTR));
        label_8->setText(QApplication::translate("tttuner", "kHz", Q_NULLPTR));
        streamGroupBox->setTitle(QApplication::translate("tttuner", "Stream Type", Q_NULLPTR));
        rtpButton->setText(QApplication::translate("tttuner", "RTP", Q_NULLPTR));
        udpButton->setText(QApplication::translate("tttuner", "UDP", Q_NULLPTR));
        IPlineEdit->setText(QApplication::translate("tttuner", "127.0.0.1", Q_NULLPTR));
        label_9->setText(QApplication::translate("tttuner", "Width", Q_NULLPTR));
        label_10->setText(QApplication::translate("tttuner", "x", Q_NULLPTR));
        label_11->setText(QApplication::translate("tttuner", "Height", Q_NULLPTR));
        label_12->setText(QApplication::translate("tttuner", "Scale", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class tttuner: public Ui_tttuner {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TTTUNER_H
