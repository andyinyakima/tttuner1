#-------------------------------------------------
#
# Project created by QtCreator 2013-02-04T11:10:44
#
#-------------------------------------------------

CONFIG  += qt debug
QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TTTuner1
TEMPLATE = app


SOURCES += main.cpp\
        tttuner.cpp

HEADERS  += tttuner.h

FORMS    += tttuner.ui

QMAKE_LFLAGS += -no-pie
QMAKE_CXXFLAGS += -std=c++11

TARGET.path = /usr/local/bin
TARGET.files = TTTuner1
INSTALLS += TARGET
